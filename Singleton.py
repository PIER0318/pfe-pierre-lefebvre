import logging


class MetaSingleton(type):
    """
    Patron de conception pour la création de singletons.
    """

    __instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in MetaSingleton.__instances:
            logging.info(f'Creating new instance of {cls}')
            MetaSingleton.__instances[cls] = super(MetaSingleton, cls).__call__(*args, **kwargs)
        return MetaSingleton.__instances[cls]
