# Projet de fin d'études : "Détection et suivi des évênements et d'objets dans un système de vidéosurveillance distribué"


## Description

Ce répertoire contient le programme réalisé dans le cadre de mon projet de fin d'étude réalisé au sein du De Vinci 
Research Center (DVRC) intitulé "Détection et suivi des évênements et d'objets dans un système de vidéosurveillance 
distribué". 

L'objectif est de détecter des personnes/objets dans une vidéo ainsi que les relations spatio-temporelles
entre ces derniers et de les stocker dans une base de données de graphes à l'aide de l'interface neo4j.

## Dépendances 

* tensorflow-gpu (https://www.tensorflow.org/)
* deep-sort-realtime (https://pypi.org/project/deep-sort-realtime/)
* neo4j (https://neo4j.com/fr/)
* mediapipe (https://google.github.io/mediapipe/)
* pytorch (https://pytorch.org/)
* pixellib (https://github.com/ayoolaolafenwa/PixelLib)
* DeepSort (https://github.com/KaiyangZhou/deep-person-reid)
* face-recognition (https://pypi.org/project/face-recognition/)

(voir rapport pour un guide détaillé de la création de l'environnement d'exécution)

## Auteur
 
Pierre Lefebvre (LEFP18039809)
