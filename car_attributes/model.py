"""
This code is implemented from these of two papers:
- Q. Hu, H. Wang, T. Li and C. Shen, "Deep CNNs With Spatially Weighted Pooling for Fine-Grained Car Recognition,"
in IEEE Transactions on Intelligent Transportation Systems, vol. 18, no. 11, pp. 3147-3156, Nov. 2017, doi: 10.1109/TITS.2017.2679114.
- Yang, L., Luo, P., Loy, C. C., & Tang, X. (2015). A large-scale car dataset for fine-grained categorization and verification.
2015 IEEE Conference on Computer Vision and Pattern Recognition (CVPR). doi:10.1109/cvpr.2015.7299023
"""

import tensorflow as tf
from tensorflow.keras.applications import ResNet50, ResNet101, VGG16
from tensorflow.keras.layers import BatchNormalization, Activation, Flatten, Dense, Dropout, Conv2D, MaxPooling2D
from tensorflow.keras.regularizers import L2
from tensorflow.keras.initializers import random_normal
from car_attributes.layers import SWPLayer


class ResNet_SWP(tf.keras.Model):
    def __init__(self,
                 base_model: str = 'resnet50',
                 base_model_trainable: bool = True,
                 # input_shape: tuple = (224, 224, 3),
                 input_shape: tuple = (160, 160, 3),  # modification de input_shape
                 num_classes: int = 431,
                 swp_num_of_masks: int = 9,
                 fc_nodes: int = 1024,
                 weight_decay: float = 0.0005,
                 stddev: float = 0.005,
                 dropout_ratio: float = 0.5,
                 random_state: int = None
                 ):
        """
        # ResNet_SWP
        `base_model` (str): resnet50 or resnet101
        `base_model_trainable` (bool): set `base_model` to `trainable`
        `input_shape` (tuple): input shape of images
        `num_classes` (int): number of classes
        `swp_num_of_masks` (int): number of masks of SWP layer
        `fc_nodes` (int): number of nodes of fully-connected layers
        `weight_decay` (float): L2 regularized multiplier
        `stddev` (float): standard deviation of random normal
        `dropout_ratio` (float): ratio of Dropout layer to prevent overfitting problem
        `random_state` (int): set random state for random initialized parameters.
        """
        super(ResNet_SWP, self).__init__()

        # initialize layers
        if base_model == 'resnet50':
            self.base_model = ResNet50(include_top=False, input_shape=input_shape, weights=None)
        else:
            self.base_model = ResNet101(include_top=False, input_shape=input_shape, weights=None)

        self.base_model.trainable = base_model_trainable

        self.swp_layer = SWPLayer(swp_num_of_masks, seed=random_state)
        self.batch_norm_layer_1 = BatchNormalization()
        self.batch_norm_layer_2 = BatchNormalization()
        self.relu_act_layer_1 = Activation('relu')
        self.relu_act_layer_2 = Activation('relu')
        self.flatten_layer = Flatten()
        self.fc_layer = Dense(fc_nodes, kernel_regularizer=L2(weight_decay),
                              kernel_initializer=random_normal(stddev=stddev, seed=random_state))
        self.dropout_layer = Dropout(dropout_ratio, seed=random_state)
        self.output_layer = Dense(num_classes,
                                  kernel_regularizer=L2(weight_decay),
                                  activation='softmax')

    def call(self, inputs):
        x = self.base_model(inputs)
        x = self.swp_layer(x)
        x = self.batch_norm_layer_1(x)
        x = self.flatten_layer(x)
        x = self.relu_act_layer_1(x)
        x = self.fc_layer(x)
        x = self.batch_norm_layer_2(x)
        x = self.relu_act_layer_2(x)
        x = self.dropout_layer(x)
        return self.output_layer(x)
