import time
import logging

from KeyFrame import KeyFrameExtractor, KeyFrameDetections, KeyFrameSaver
from Detectors import InstanceSegmentationDetector, DepthDetector, PoseDetector, FacialAttributesDetector, \
    CarAttributesDetector
from Reidentification import Reidentifier
from Relationships import SpatialRelationshipBuilder
from Database import Neo4jConnection

# ----------------------------------------------------------------------------------------------------------------------
# ---   Constants                                                                                                    ---
# ----------------------------------------------------------------------------------------------------------------------

# --- KeyFrame Extraction ---
#INPUT_PATH = './data/SCVD/videos/Non-Violence Videos/nv187.MOV'
INPUT_PATH = './data/rtlab/violence-detection-dataset/non-violent/cam1/1.mp4'
KF_THRESHOLD = 50

# --- Instance Segmentation ---
WEIGHTS_PATH = './weights/pointrend/pointrend_resnet50.pkl'
CLASSES_PATH = './weights/pointrend/classes_short.names'
BACKBONE_TYPE = 'resnet50'
DETECTION_SPEED = 'fast'
INS_CONF = 0.5

# --- Monocular Depth ---
DEPTH_WEIGHTS_PATH = './weights/monocular_depth/dense_depth_weights.h5'

# --- Attributes Detections ---
POSE_CONF = 0.5
VISIBILITY_THRESH = 0.5
FACE_WEIGHTS_PATH = './weights/person/face_attributes_weights.h5'
CAR_WEIGHTS_PATH = './weights/car/car_attributes_weights.h5'
CAR_CLASSES_PATH = './weights/car/car_names.csv'

# --- Spatial Relationships ---
SRB_MARGIN = 10

# --- Save ---
COLORS_PATH = './weights/pointrend/colors.json'
OUTPUT_PATH = './results/'

# --- Reidentification ---
OSNET_WEIGHTS_PATH = './weights/torchreid/osnet_x1_0_imagenet.pth'
REID_DEPTH = 10


def main():

    logging.basicConfig(level=logging.INFO, filename="debug.log", filemode="w",
                        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    kf_extractor = KeyFrameExtractor(input_path=INPUT_PATH, kf_threshold=KF_THRESHOLD)

    ins_det = InstanceSegmentationDetector(weights_path=WEIGHTS_PATH, backbone_type=BACKBONE_TYPE,
                                           classes_path=CLASSES_PATH, conf=INS_CONF, detection_speed=DETECTION_SPEED)

    kf_detections = KeyFrameDetections()

    depth_detector = DepthDetector(weights_path=DEPTH_WEIGHTS_PATH)

    pose_detector = PoseDetector(conf=POSE_CONF, visibility_thresh=VISIBILITY_THRESH)
    person_detector = FacialAttributesDetector(FACE_WEIGHTS_PATH)
    car_detector = CarAttributesDetector(weights_path=CAR_WEIGHTS_PATH, classes_path=CAR_CLASSES_PATH)

    reidentifier = Reidentifier(depth=REID_DEPTH, weights_path=OSNET_WEIGHTS_PATH)

    srb = SpatialRelationshipBuilder(margin=SRB_MARGIN)

    saver = KeyFrameSaver(output_path=OUTPUT_PATH, colors_path=COLORS_PATH)
    saver.clean_output_folder()

    connection = Neo4jConnection(uri="bolt://localhost:7687", user="pierre", pwd="18031998",
                                 cam_id="21616512165", classes_path=CLASSES_PATH)
    connection.query("CREATE OR REPLACE DATABASE video")

    prev_frame = 0

    while kf_extractor.cap_is_opened():

        try:
            img, i_frame = kf_extractor.extract_next_kf(prev_frame)

        except Exception as e:
            print("Exception occurred:", str(e))

        else:

            t1 = time.time()
            outputs = ins_det.detect(img)
            ins_det.post_process(img, outputs, kf_detections)

            depth_map = depth_detector.infer(img)
            depth_detector.post_process(depth_map, kf_detections)

            if kf_detections.get_nb_det() > 0:
                kf_detections.init_attributes()
                labels = kf_detections.get_labels()
                extracted_objects = kf_detections.get_extracted_objects()
                masks = kf_detections.get_masks()

                for i in range(kf_detections.get_nb_det()):

                    if labels[i] == 'person':
                        face_outputs = person_detector.infer(extracted_objects[i], masks[i])
                        person_detector.post_process(kf_detections, face_outputs, i)
                        angles_outputs = pose_detector.infer(extracted_objects[i], masks[i])
                        pose_detector.post_process(kf_detections, angles_outputs, i)

                    elif labels[i] == 'car':
                        car_outputs = car_detector.infer(extracted_objects[i], masks[i])
                        car_detector.post_process(kf_detections, car_outputs, i)

                reidentifier.compute_new_ids(img, kf_detections)
                srb.make_relationships(kf_detections)

            connection.build_graph_v1(kf_detections, i_frame, prev_frame)
            #connection.build_graph_v2(kf_detections, i_frame, prev_frame)

            logging.info(f"Keyframe done ({round(time.time() - t1, 2)} s)")

            saver.save_kf_detections(img, kf_detections, i_frame, True, True)
            saver.save_depth_map(depth_map, i_frame)

            prev_frame = i_frame

        finally:
            print(time.time() - t1)


if __name__ == '__main__':
    main()
