import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.layers import LeakyReLU


class FaceAttributesModel(Model):

    def __init__(self, input_shape=(48, 48, 3)):
        super(FaceAttributesModel, self).__init__()
        input = tf.keras.layers.Input(input_shape)
        x = tf.keras.applications.vgg16.preprocess_input(input)
        backbone = VGG16(
            include_top=False, weights='imagenet',
            input_tensor=x
        )
        output_layer = backbone.get_layer("block5_conv3").output
        x = tf.keras.layers.Flatten()(output_layer)
        output_age = self.build_age_branch(x)
        output_ethnicity = self.build_etchnicity_branch(x)
        output_gender = self.build_gender_branch(x)
        self.model = tf.keras.Model(inputs=input, outputs=[output_age, output_ethnicity, output_gender])

    def call(self, x):
        return self.model(x)

    def build_age_branch(self, input_tensor):
        x = tf.keras.layers.Dense(1024, activation=LeakyReLU(alpha=0.3))(input_tensor)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.Dropout(0.2)(x)
        x = tf.keras.layers.Dense(1, activation=None, name='age_output')(x)
        return x

    def build_etchnicity_branch(self, input_tensor):
        x = tf.keras.layers.Dense(500, activation=LeakyReLU(alpha=0.3))(input_tensor)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.Dropout(0.2)(x)
        x = tf.keras.layers.Dense(5, activation='softmax', name='ethnicity_output')(x)
        return x

    def build_gender_branch(self, input_tensor):
        x = tf.keras.layers.Dense(500, activation=LeakyReLU(alpha=0.3))(input_tensor)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.Dropout(0.2)(x)
        x = tf.keras.layers.Dense(1, activation='sigmoid', name='gender_output')(x)
        return x
