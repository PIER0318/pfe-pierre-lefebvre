from Singleton import MetaSingleton


class SpatialRelationshipBuilder(metaclass=MetaSingleton):
    """
    Cette classe permet de générer les relations spatiales entre les objets détectés.
    """

    def __init__(self, margin):
        """
        Constructeur de SpatialRelationshipBuilder.
        :param margin: intervalle de confiance pour les relations d'Allen
        """

        self.margin = margin


    def make_relationships(self, kf_detections):
        """
        Méthode effectuant l'actualisation des relations spatiales dans l'instance courante de KeyFrameDetections.
        :param kf_detections: instance courante de KeyFrameDetections
        :return:
        """

        rel = []
        boxes = kf_detections.get_boxes()
        depths = kf_detections.get_depths()
        ids = kf_detections.get_ids()
        for i in range(kf_detections.get_nb_det() - 1):
            for j in range(i + 1, kf_detections.get_nb_det()):
                rel.append([ids[i], ids[j], self.get_bb_reltype(boxes[i], boxes[j], depths[i], depths[j])])
        kf_detections.set_relationships(rel)


    def get_bb_reltype(self, bb1, bb2, depth1, depth2):
        """
        Méthode retournant le type de relation spatiale entre deux bb dont le préfixe correspond à la profondeur
        relative et le suffixe à la relation de Allen.
        :param bb1: première bb (np.ndarray)
        :param bb2: deuxième bb (np.ndarray)
        :param depth1: profondeur de la première bb (float)
        :param depth2: profondeur de la deuxième bb (float)
        :return: un relation spatiale (str)
        """

        return self.get_depth_prefix(depth1, depth2) + "_" + self.get_allen_suffix(bb1, bb2)


    def get_depth_prefix(self, depth1, depth2):
        """
        Méthode permettant la détermination du préfixe correpondant à la profondeur relative entre deux bb : In Front
        Of (IFO), Same Plan (SP) ou Behind (BE).
        :param depth1: profondeur de la première bb (float)
        :param depth2: profondeur de la deuxième bb (float)
        :return: le préfixe (str)
        """

        if round(depth1, 1) > round(depth2, 1):
            return "IFO"
        elif round(depth1, 1) == round(depth2, 1):
            return "SP"
        else:
            return "BE"


    def get_allen_suffix(self, bb1, bb2):
        """
        Méthode permettant la détermination du suffixe correspondant à la relation de Allen entre deux bb : Equal (EQ),
        Tangential Proper Part (TPP), Tangential Proper Part inverse (TPPi), Non Tangential Proper Part (NTPP), Non
        Tangential Proper Part Invert (NTPPi), Externally Connected (EC) Partial Overlapping (PO), Disconnected (DC).
        :param bb1: première bb (np.ndarray)
        :param bb2: deuxième bb (np.ndarray)
        :return: le suffixe d'Allen (str)
        """

        ax = (bb1[0], bb1[2])
        ay = (bb1[1], bb1[3])
        bx = (bb2[0], bb2[2])
        by = (bb2[1], bb2[3])

        if self.eq(ax, ay, bx, by):
            return "EQ"
        elif self.tpp(ax, ay, bx, by):
            return "TPP"
        elif self.tpp(bx, by, ax, ay):
            return "TPPi"
        elif self.ntpp(ax, ay, bx, by):
            return "NTPP"
        elif self.ntpp(bx, by, ax, ay):
            return "NTPPi"
        elif self.ec(ax, ay, bx, by):
            return "EC"
        elif self.po(ax, ay, bx, by):
            return "PO"
        elif self.dc(ax, ay, bx, by):
            return "DC"


    @staticmethod
    def before(c, d):
        """
        Méthode permettant de déterminer si le côté c se trouve avant le côté d selon l'axe x ou y.
        :param c: premier côté (tuple(int, int))
        :param c: deuxième côté (tuple(int, int))
        :return: un booléen
        """

        return c[1] < d[0]


    def meets(self, c, d):
        """
        Méthode permettant de déterminer si le côté c rencontre le côté d selon l'axe x ou y.
        :param c: premier côté (tuple(int, int))
        :param c: deuxième côté (tuple(int, int))
        :return: un booléen
        """

        return d[0] - self.margin <= c[1] <= d[0] + self.margin


    def starts(self, c, d):
        """
        Méthode permettant de déterminer si le côté c débute le côté d selon l'axe x ou y.
        :param c: premier côté (tuple(int, int))
        :param c: deuxième côté (tuple(int, int))
        :return: un booléen
        """

        return d[0] - self.margin <= c[0] <= d[0] + self.margin


    def finishes(self, c, d):
        """
        Méthode permettant de déterminer si le côté c termine le côté d selon l'axe x ou y.
        :param c: premier côté (tuple(int, int))
        :param c: deuxième côté (tuple(int, int))
        :return: un booléen
        """

        return d[1] - self.margin <= c[1] <= d[1] + self.margin


    def during(self, c, d):
        """
        Méthode permettant de déterminer si le côté c est inclu dans le côté d selon l'axe x ou y.
        :param c: premier côté (tuple(int, int))
        :param c: deuxième côté (tuple(int, int))
        :return: un booléen
        """

        return c[0] >= d[0] - self.margin and c[1] <= d[1] + self.margin


    @staticmethod
    def overlaps(c, d):
        """
        Méthode permettant de déterminer si le côté c se superpose au côté d selon l'axe x ou y.
        :param c: premier côté (tuple(int, int))
        :param c: deuxième côté (tuple(int, int))
        :return: un booléen
        """

        return c[0] <= d[0] <= c[1] <= d[1] or d[0] <= c[0] <= d[1] <= c[1]


    def dc(self, ax, ay, bx, by):
        """
        Méthode permettant de déterminer si deux bb sont disjointes.
        :param ax: côté de la bb a en x
        :param ay: côté de la bb a en y
        :param bx: côté de la bb b en x
        :param by: côté de la bb b en y
        :return: un booléen
        """

        if self.before(ax, bx) or self.before(ay, by) or self.before(bx, ax) or self.before(by, ay):
            return True
        return False


    def ec(self, ax, ay, bx, by):
        """
        Méthode permettant de déterminer si deux bb se touchent mais ne se superposent pas.
        :param ax: côté de la bb a en x
        :param ay: côté de la bb a en y
        :param bx: côté de la bb b en x
        :param by: côté de la bb b en y
        :return: un booléen
        """

        if (self.meets(ax, bx) or self.meets(bx, ax)) and not (self.before(ay, by) or self.before(by, ay)) or \
                (self.meets(ay, by) or self.meets(by, ay)) and not (self.before(ax, bx) or self.before(bx, ax)):
            return True
        return False


    def eq(self, ax, ay, bx, by):
        """
        Méthode permettant de déterminer si deux bb se superposent parfaitement.
        :param ax: côté de la bb a en x
        :param ay: côté de la bb a en y
        :param bx: côté de la bb b en x
        :param by: côté de la bb b en y
        :return: un booléen
        """

        if self.starts(ax, bx) and self.finishes(ax, bx) and self.starts(ay, by) and self.finishes(ay, by):
            return True
        return False


    def po(self, ax, ay, bx, by):
        """
        Méthode permettant de déterminer si deux bb se superposent.
        :param ax: côté de la bb a en x
        :param ay: côté de la bb a en y
        :param bx: côté de la bb b en x
        :param by: côté de la bb b en y
        :return: un booléen
        """

        if (self.overlaps(ax, bx) or self.overlaps(bx, ax)) and not (self.before(ay, by) or self.before(by, ay)) or \
                (self.overlaps(ay, by) or self.overlaps(by, ay)) and not (self.before(ax, bx) or self.before(bx, ax)) or \
                self.during(ax, bx) and self.during(by, ay) or self.during(bx, ax) and self.during(ay, by):
            return True
        return False


    def tpp(self, ax, ay, bx, by):
        """
        Méthode permettant de déterminer si la bb a est incluse dans la bb b et touche un de ses côtés.
        :param ax: côté de la bb a en x
        :param ay: côté de la bb a en y
        :param bx: côté de la bb b en x
        :param by: côté de la bb b en y
        :return: un booléen
        """

        if (self.during(ax, bx) and (self.starts(ay, by) or self.finishes(ay, by))) or \
                (self.during(ay, by) and (self.starts(ax, bx) or self.finishes(ax, bx))):
            return True
        return False


    def ntpp(self, ax, ay, bx, by):
        """
        Méthode permettant de déterminer si la bb a est incluse dans la bb b sans toucher un de ses côtés.
        :param ax: côté de la bb a en x
        :param ay: côté de la bb a en y
        :param bx: côté de la bb b en x
        :param by: côté de la bb b en y
        :return: un booléen
        """

        if (self.during(ax, bx) and not (self.starts(ay, by) or self.finishes(ay, by))) and \
                (self.during(ay, by) and not (self.starts(ax, bx) or self.finishes(ax, bx))):
            return True
        return False
