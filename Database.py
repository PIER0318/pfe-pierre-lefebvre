from neo4j import GraphDatabase
from Singleton import MetaSingleton


class Neo4jConnection(metaclass=MetaSingleton):
    """
    Cette classe permet l'envoi de requêtes à la BDD afin d'incrémenter le graphes avec les nouvelles
    détections/relations.
    """

    def __init__(self, uri, user, pwd, cam_id, classes_path, fps=25):
        """
        Constructeur de la classe Neo4jConnection.
        :param uri: uri de la BDD (str)
        :param user: identifiant de l'utilisateur de la BDD (str)
        :param pwd: mot de passe de l'utilisateur de la BDD (str)
        :param cam_id: identifiant de la caméra (str)
        :param classes_path: chemin relatif des classes du modèle de segmentation d'instance (str)
        :param fps: cadence d'images de la caméra (int)
        """

        self.__uri = uri
        self.__user = user
        self.__pwd = pwd
        self.__cam_id = cam_id
        self.__classes = open(classes_path).read().strip().split("\n")
        self.__fps = fps
        self.__driver = None
        try:
            self.__driver = GraphDatabase.driver(self.__uri, auth=(self.__user, self.__pwd))
        except Exception as e:
            print("Failed to create the driver:", e)


    def close(self):
        """
        Méthode permettant la fermeture du pilote de la BDD.
        """

        if self.__driver is not None:
            self.__driver.close()


    def query(self, query, db=None):
        """
        Méthode permettant l'envoie de requêtes à la BDD.
        :param query: requête à envoyer (str)
        :param db: instance de la BDD (str)
        :return:
        """

        assert self.__driver is not None, "Driver not initialized!"
        session = None
        response = None
        try:
            session = self.__driver.session(database=db) if db is not None else self.__driver.session()
            response = list(session.run(query))
        except Exception as e:
            print("Query failed:", e)
        finally:
            if session is not None:
                session.close()
        return response


    def build_graph_v1(self, kf_detections, i_frame, prev_frame):
        """
        Méthode permettant la formulation d'une requête pour l'ajout des détections/relations d'une keyframe au graphe
        de la BDD (format V1).
        :param kf_detections: instance de KeyFrameDetections comportant les détections/relations pour la keyframe
        courante
        :param i_frame: indice de la keyframe courante
        :param prev_frame: indice de la keyframe précédente
        :return:
        """

        if prev_frame == 0:
            self.query(f'''CREATE (:Cam {{label: "cam", id: {self.__cam_id}}})''')
            self.query(f'''MATCH (c: Cam)
            WHERE c.id = {self.__cam_id}
            CREATE (c)-[:BELONGS_TO]->(:KFrame {{label: "keyframe", nb: {i_frame}}})''')
        else:
            self.query(f'''MATCH (c: Cam)--(kf: KFrame)
            WHERE c.id = {self.__cam_id} AND kf.nb = {prev_frame}
            CREATE (c)-[:BELONGS_TO]->(:KFrame {{label: "keyframe", nb: {i_frame}}})<-[:AFTER {{time: {(i_frame - prev_frame) * self.__fps}}}]-(kf)''')
        for i in range(kf_detections.get_nb_det()):
            self.query(f'''MATCH (c: Cam)--(kf: KFrame)
            WHERE c.id = {self.__cam_id} AND kf.nb = {i_frame}
            CREATE (kf)-[:BELONGS_TO]->(:Detection {kf_detections.node_to_string_v1(i)})''')
        for relationship in kf_detections.get_relationships():
            self.query(f'''MATCH (c: Cam)--(kf: KFrame), (kf: KFrame)--(d1: Detection), (kf: KFrame)--(d2: Detection)
            WHERE c.id = {self.__cam_id} AND kf.nb = {i_frame} AND d1.id = {relationship[0]} AND d2.id = {relationship[1]}
            CREATE (d1)-[:{relationship[2]}]->(d2)''')


    def build_graph_v2(self, kf_detections, i_frame, prev_frame):
        """
        Méthode permettant la formulation d'une requête pour l'ajout des détections/relations d'une keyframe au graphe
        de la BDD (format V2).
        :param kf_detections: instance de KeyFrameDetections comportant les détections/relations pour la keyframe
        courante
        :param i_frame: indice de la keyframe courante
        :param prev_frame: indice de la keyframe précédente
        :return:
        """

        if prev_frame == 0:
            self.query(f'''CREATE (:Cam {{label: "cam", id: {self.__cam_id}}})''')
            self.query(f'''MATCH (c: Cam)
            WHERE c.id = {self.__cam_id}
            CREATE (c)-[:BELONGS_TO]->(:KFrame {{label: "keyframe", nb: {i_frame}}})''')
        else:
            self.query(f'''MATCH (c: Cam)--(kf: KFrame)
            WHERE c.id = {self.__cam_id} AND kf.nb = {prev_frame} 
            CREATE (c)-[:BELONGS_TO]->(:KFrame {{label: "keyframe", nb: {i_frame}}})<-[:AFTER {{time: {(i_frame - prev_frame) * self.__fps}}}]-(kf)''')
        for i in range(kf_detections.get_nb_det()):
            self.query(f'''MATCH (c: Cam)--(kf: KFrame)
            WHERE c.id = {self.__cam_id} AND kf.nb = {i_frame}
            MERGE (det :Detection {{id: {kf_detections.get_ids()[i]}, label: '{kf_detections.get_labels()[i]}'}})
            CREATE (kf)-[:BELONGS_TO {kf_detections.rel_to_string_v2(i)}]->(det)''')
        for relationship in kf_detections.get_relationships():
            self.query(f'''MATCH (c: Cam)--(kf: KFrame), (kf: KFrame)--(d1: Detection), (kf: KFrame)--(d2: Detection)
            WHERE c.id = {self.__cam_id} AND kf.nb = {i_frame} AND d1.id = {relationship[0]} AND d2.id = {relationship[1]}
            MERGE (d1)-[r:{relationship[2]}]->(d2)
                ON CREATE SET r.kf = [{i_frame}]
                ON MATCH SET r.kf = r.kf + {i_frame}''')
