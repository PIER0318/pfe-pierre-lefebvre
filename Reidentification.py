from Singleton import MetaSingleton
from deep_sort_realtime.deepsort_tracker import DeepSort
import numpy as np


class Reidentifier(metaclass=MetaSingleton):
    """
    Cette classe permet la réidentification des objets détectés.
    """

    def __init__(self, depth, weights_path):
        """
        Constructeur de Reidentifier.
        :param depth: nombre maximum de keyframes pour lequel un objet est gardé en mémoire sans réapparaître
        :param weights_path: chemin relatif des poids du modèle de réidentification
        """
        self.__tracker = DeepSort(max_age=depth, embedder='torchreid', embedder_model_name='osnet_x1_0',
                                  max_cosine_distance=0.5, embedder_wts=weights_path)


    def compute_new_ids(self, img, kf_detections):
        """
        Méthode effectuant le tracking des bounding boxes et l'actualisation des ids dans l'instance courante de
        KeyFrameDetections.
        :param img: keyframe (np.ndarray)
        :param kf_detections: instance courante de KeyFrameDetections
        """

        boxes = kf_detections.get_boxes()
        confs = kf_detections.get_confs()
        labels = kf_detections.get_labels()
        nb_det = kf_detections.get_nb_det()
        raw_det = self.__reformat_boxes__(boxes, confs, labels, nb_det)

        tracks = self.__tracker.update_tracks(raw_detections=raw_det, frame=img)

        ids = []
        ltrbs = []
        for track in tracks:
            ids.append(int(track.track_id))
            ltrbs.append(track.to_ltrb(orig=True))

        ids = self.__reorder_ids__(ids, boxes, ltrbs)

        kf_detections.set_ids(ids)


    def __reorder_ids__(self, ids, boxes, ltrbs):
        """
        Méthode permettant de réorganiser les ids.
        :param ids: liste des ids
        :param boxes: bounding boxes
        :param ltrbs: bounding boxes réidentifiées
        :return:
        """
        sorted_ids = np.zeros(len(ids), dtype=np.int32)
        for i in range(len(boxes)):
            for j in range(len(ltrbs)):
                if self.__same_boxes__(boxes[i], ltrbs[j]):
                    sorted_ids[i] = ids[j]
        return sorted_ids


    @staticmethod
    def __same_boxes__(bb1, bb2):
        """
        Méthode permettant de comparer deux bounding boxes.
        :param bb1: première bounding box
        :param bb2: deuxième bounding box
        :return: un booléen indiquant si deux bb sont identiques
        """

        for i in range(4):
            if bb1[i] != bb2[i]:
                return False
        return True


    @staticmethod
    def __reformat_boxes__(boxes, confs, labels, nb_det):
        """
        Méthode permettant de mettre les bounding boxes au format [x, y, w, h].
        :param boxes: bounding boxes (np.ndarray)
        :param confs: niveaux de confiance des détections
        :param labels: labels des détections
        :param nb_det: nombre de détections
        :return: une structure comportant les bb, les niveaux de confiances et les labels
        """

        raw_det = []
        for i in range(nb_det):
            x = boxes[i][0]
            y = boxes[i][1]
            w = boxes[i][2] - boxes[i][0]
            h = boxes[i][3] - boxes[i][1]
            raw_det.append(([x, y, w, h], confs[i], labels[i]))
        # print(raw_det)
        return raw_det
