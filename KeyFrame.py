import glob
import os
from Singleton import MetaSingleton
import cv2
import numpy as np
import json


class KeyFrameExtractor(metaclass=MetaSingleton):
    """
    Cette classe permet l'extraction des keyframes à partir d'un flux vidéo en utilisant l'algorithme ORB + KNN.
    """

    def __init__(self, input_path, dist_ratio=0.7, kf_threshold=50, prev_des=None):
        """
        Constructeur de KeyFrameExtractor.
        :param input_path: chemin relatif vers la vidéo (str)
        :param dist_ratio: ratio de distance (float)
        :param kf_threshold: nombre maximum de points clés pour considérer une nouvelle keyframe (int)
        :param prev_des: descripteur des points clés de la keyframe précédente (np.ndarray)
        """

        self.__cap = cv2.VideoCapture(input_path)
        self.__orb = cv2.ORB_create()
        self.__bf = cv2.BFMatcher()
        self.__dist_ratio = dist_ratio
        self.__kf_threshold = kf_threshold
        self.__prev_des = prev_des


    def cap_is_opened(self):
        """
        Méthode retournant l'état du lecteur de vidéo.
        :return: état du lecteur de vidéo (bool)
        """

        return self.__cap.isOpened()


    def extract_next_kf(self, i_frame):
        """
        Méthode permetant l'extraction de la keyframe suivante.
        :param i_frame: indice de la frame courante (int)
        :return: image (np.ndarray) et indice (int) de la keyframe suivante
        """

        if i_frame == 0:
            success, img = self.__cap.read()

            if success:
                self.__prev_des = self.__orb.detectAndCompute(img, None)[1]
                i_frame += 1
                return img, i_frame

            else:
                self.__cap.release()
                raise Exception("Couldn't read Video!")

        while True:
            success, img = self.__cap.read()

            if success:
                des = self.__orb.detectAndCompute(img, None)[1]
                i_frame += 1
                matches = self.__bf.knnMatch(self.__prev_des, des, k=2)
                good = []

                for m, n in matches:
                    if m.distance < self.__dist_ratio * n.distance:
                        good.append([m])

                # print(len(good))

                if len(good) < self.__kf_threshold:
                    self.__prev_des = des
                    return img, i_frame

            else:
                self.__cap.release()
                raise Exception("End of video!")


class KeyFrameSaver(metaclass=MetaSingleton):
    """
    Cette classe permet de sauvegarder l'image des keyframes au format JPG en faisant apparaître les détections.
    """

    def __init__(self, output_path, colors_path, transparency_factor=0.7):
        """
        Constructeur de KeyFrameSaver.
        :param output_path: chemin relatif du répertoire de sauvegarde (str)
        :param colors_path: chemin relatif des couleurs des classes du modèle de segmentation d'instances (str)
        :param transparency_factor: facteur de transparence pour la segmentation d'instance (float)
        """

        self.__output_path = output_path
        self.__colors = json.load(open(colors_path))
        self.alpha = transparency_factor


    def clean_output_folder(self):
        """
        Méthode permettant de supprimer les fichiers présents dans le répertoire de sauvegarde.
        """

        if not os.path.exists(self.__output_path):
            os.makedirs(self.__output_path)
        else:
            files = glob.glob(self.__output_path + '*')
            for f in files:
                os.remove(f)


    def __draw_bb__(self, img, bb, labels, id):
        """
        Méthode permettant de dessiner une bounding box autour d'un objet détecté dans une image en faisant apparaître
        son label et son id.
        :param img: une image (np.ndarray)
        :param bb: bounding box de l'objet (np.ndarray)
        :param labels: label de l'objet (np.ndarray)
        :param id: id de l'objet (int)
        :return:
        """

        text = labels + " (id: %d)" % id
        text_size = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, 0.4, 1)
        dim, _ = text_size[0], text_size[1]
        cv2.rectangle(img, (bb[0], bb[1]), (bb[2], bb[3]), self.__colors[labels], 2, cv2.LINE_AA)
        cv2.rectangle(img, (bb[0] - 1, bb[1] - dim[1] - 2), (bb[0] + dim[0], bb[1]), self.__colors[labels],
                      cv2.FILLED)
        cv2.putText(img, text, (bb[0], bb[1] - 2), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 0), 1, cv2.LINE_AA)


    def __draw_poly__(self, img, poly, labels):
        """
        Méthode permettant de dessiner un polygone en transparence autour d'un objet détecté dans une image.
        :param img: une image (np.ndarray)
        :param poly: polygone de l'objet (np.ndarray)
        :param labels: label de l'objet (np.ndarray)
        :return:
        """
        overlay = img.copy()
        cv2.polylines(overlay, poly, True, color=(0, 0, 0), thickness=2)
        cv2.fillPoly(overlay, poly, color=self.__colors[labels])
        cv2.addWeighted(overlay, self.alpha, img, 1 - self.alpha, 0, img)


    def save_kf_detections(self, img, kf_detections, i_frame, draw_rec=True, draw_poly=False):
        """
        Méthode permettant de dessiner les bounding boxes et polygones des objets détectés dans une keyframe puis de
        sauvegarder cette image dans le répertoire de sauvegarde.
        :param img: keyframe (np.ndarray)
        :param kf_detections: instance courante de KeyFrameDetections
        :param i_frame: indice de la keyframe courante (int)
        :param draw_rec: choix de faire apparaître les bounding boxes (bool)
        :param draw_poly: choix de faire apparaître la segmentation d'instances (bool)
        """

        boxes = kf_detections.get_boxes()
        polygons = kf_detections.get_poly()
        labels = kf_detections.get_labels()
        nb_det = kf_detections.get_nb_det()
        ids = kf_detections.get_ids()

        for i in range(nb_det):
            if draw_rec:
                self.__draw_bb__(img, boxes[i], labels[i], ids[i])
            if draw_poly:
                self.__draw_poly__(img, polygons[i], labels[i])

        cv2.imwrite(self.__output_path + "/frame_%d.jpg" % i_frame, img)


    def save_depth_map(self, depth_map, i_frame):
        """
        Méthode permettant de sauvegarder les cartes de profondeur des keyframes.
        :param depth_map: carte de profondeur (np.ndarray)
        :param i_frame: indice de la keyframe courante (int)
        """

        im_out = cv2.cvtColor(depth_map * 255, cv2.COLOR_GRAY2BGR)
        im_out = cv2.applyColorMap(im_out.astype(np.uint8), cv2.COLORMAP_PLASMA)
        cv2.imwrite(self.__output_path + "/depth_map_%d.jpg" % i_frame, im_out)


class KeyFrameDetections:
    """
    Cette classe permet de regrouper les détections des keyframes ainsi que leurs attributs.
    """

    def __init__(self):
        """
        Constructeur de KeyFrameDetections.
        """

        self.__boxes = []
        self.__labels = []
        self.__object_counts = []
        self.__confidences = []
        self.__polygons = []
        self.__masks = []
        self.__extracted_objects = []

        self.__depths = []

        self.__attributes = []
        self.__ids = []
        self.__relationships = []


    def get_boxes(self):
        """
        getter de __boxes.
        :return: la liste des bounding boxes
        """

        return self.__boxes


    def set_boxes(self, boxes):
        """
        setter de __labels.
        :param boxes: une liste de bounding boxes
        """

        self.__boxes = boxes


    def get_labels(self):
        """
        getter de __labels.
        :return: la liste des labels
        """

        return self.__labels


    def set_labels(self, labels):
        """
        setter de __labels.
        :param labels: une liste de labels
        """

        self.__labels = labels


    def get_nb_det(self):
        """
        getter de nb_det.
        :return: le nombre de détections dans une keyframe
        """

        return sum(self.__object_counts.values())


    def set_object_counts(self, object_counts):
        """
        setter du nombre de détections par type d'instance.
        :param object_counts: nombre de détections par type d'instance (list[int])
        """

        self.__object_counts = object_counts


    def get_confs(self):
        """
        getter de __confidences.
        :return: la liste des niveaux de confiance des détections
        """

        return self.__confidences

    def set_confs(self, confs):
        """
        setter de __confidences.
        :param confs: une liste de niveaux de confiance
        """

        self.__confidences = confs


    def get_poly(self):
        """
        getter de __polygons.
        :return: la liste des polygones des objets détectés
        """

        return self.__polygons


    def set_polygons(self, polygons):
        """
        setter de __polygons.
        :param polygons: une liste de polygones
        """

        self.__polygons = polygons


    def get_extracted_objects(self):
        """
        getter de __extracted_objects.
        :return: la liste des images correspondant aux objets détectés
        """

        return self.__extracted_objects


    def set_extracted_objects(self, extracted_objects):
        """
        setter de __extracted_objects.
        :param extracted_objects: une liste d'images correspondant aux objets détectés
        """

        self.__extracted_objects = extracted_objects


    def get_masks(self):
        """
        getter de __masks.
        :return: la liste des masques des objets détectés
        """

        return self.__masks


    def set_masks(self, masks):
        """
        setter de __masks.
        :param masks: une liste des masques des objets détectés
        """

        self.__masks = masks


    def get_depths(self):
        """
        getter de __depths.
        :return: la liste des profondeurs moyennes des objets détectés
        """

        return self.__depths


    def set_depths(self, depths):
        """
        setter de __depths.
        :param depths: une liste de profondeurs moyennes des objets détectés
        """

        self.__depths = depths


    def get_ids(self):
        """
        getter de __ids.
        :return: la liste des id des objets détectés
        """

        return self.__ids


    def set_ids(self, ids):
        """
        setter de __ids.
        :param ids: une liste d'id des objets détectés
        """

        self.__ids = ids


    def get_relationships(self):
        """
        getter de __relationships.
        :return: la liste des relations entre les objets détectés
        """

        return self.__relationships


    def set_relationships(self, relationships):
        """
        setter de __relationships.
        :param relationships: une liste de relations entre les objets détectés
        """

        self.__relationships = relationships


    def init_attributes(self):
        """
        Méthode pour l'initialisation de la liste des attributs des détections.
        """

        self.__attributes = [{} for _ in range(self.get_nb_det())]

    def add_attributes(self, new_att, i):
        """
        Méthode pour l'ajout d'attributs pour un objet détecté.
        :param new_att: attributs à ajouter (list[dict])
        :param i: indice de l'objet (int)
        """

        self.__attributes[i].update(new_att)


    def get_attributes(self):
        """
        getter de __attributes.
        :return: la liste des attributs pour les objets détectés
        """

        return self.__attributes


    def node_to_string_v1(self, i):
        """
        Méthode permettant d'exprimer un objet détecté dans un format compréhensible par neo4j (v1).
        :param i: indice de l'objet détecté
        :return: une chaîne de caractères
        """

        attributes_str = self.attributes_to_string(i)
        if not attributes_str:
            return f'''{{id: {self.__ids[i]}, label: "{self.__labels[i]}", bbox: {self.__boxes[i].tolist()}, conf: {self.__confidences[i]}}}'''
        return f'''{{id: {self.__ids[i]}, label: "{self.__labels[i]}", bbox: {self.__boxes[i].tolist()}, conf: {self.__confidences[i]}, ''' \
               + attributes_str \
               + '}'


    def node_to_string_v2(self, i):
        """
        Méthode permettant d'exprimer un objet détecté dans un format compréhensible par neo4j (v2).
        :param i: indice de l'objet détecté
        :return: une chaîne de caractères
        """

        return f'''{{id: {self.__ids[i]}, label: "{self.__labels[i]}"}}'''


    def rel_to_string_v2(self, i):
        """
        Méthode permettant d'exprimer une relation entre deux objets détectés dans un format compréhensible par neo4j
        (v2).
        :param i: indice de la relation
        :return: une chaîne de caractères
        """

        attributes_str = self.attributes_to_string(i)
        if not attributes_str:
            return f'''{{bbox: {self.__boxes[i].tolist()}, conf: {self.__confidences[i]}}}'''
        return f'''{{bbox: {self.__boxes[i].tolist()}, conf: {self.__confidences[i]}, ''' \
               + attributes_str \
               + '}'


    def attributes_to_string(self, i):
        """
        Méthode permettant d'exprimer les attributs d'un objet détecté dans un format compréhensible par neo4j.
        :param i: indice de l'objet détecté
        :return: une chaîne de caractères
        """

        attributes_str = ""
        attributes = self.get_attributes()[i]
        for key, value in attributes.items():
            if type(value) == str:
                attributes_str += key + ': ' + f'"{value}"' + ', '
            else:
                attributes_str += key + ': ' + f'{value}' + ', '
        return attributes_str[:-2]
