from Singleton import MetaSingleton

from monocular_depth.model import DepthEstimate
from face_attributes.model import FaceAttributesModel
from car_attributes.model import ResNet_SWP

from pixellib.torchbackend.instance import instanceSegmentation
import face_recognition
import mediapipe as mp

import pandas as pd
import numpy as np
import cv2


class InstanceSegmentationDetector(metaclass=MetaSingleton):
    """
    Cette classe permet la détection des personnes/objets présents dans une keyframe (sous forme de bounding boxes) et
    de réaliser la segmentation d'instances (sous forme de polygone + masques).
    """

    def __init__(self, weights_path, backbone_type, classes_path, conf=0.5, detection_speed='average'):
        """
        Constructeur de la classe InstanceSegmentationDetector.
        :param weights_path: chemin relatif des poids du modèle (str)
        :param backbone_type: type de backbone du modèle (str)
        :param classes_path: chemin relatif des classes du modèle (str)
        :param conf: seuil de confiance des détections (str)
        :param detection_speed: influence la rapidité et les performances du modèle (str)
        """

        self.__instance_seg = instanceSegmentation()
        self.__instance_seg.load_model(model_path=weights_path, confidence=conf, network_backbone=backbone_type,
                                       detection_speed=detection_speed)
        classes = open(classes_path).read().strip().split("\n")
        self.__target_classes = self.__init_target_classes__(classes)


    def __init_target_classes__(self, classes):
        """
        Méthode effectuant l'initialisation des classes du modèles.
        :param classes: classes du modèle (list[str])
        :return: un dictionnaire des classes retenues
        """

        target_classes = self.__instance_seg.select_target_classes()
        for cl in classes:
            if cl in target_classes.keys():
                target_classes[cl] = 'valid'
        return target_classes


    def detect(self, img):
        """
        Méthode effectuant la détection des objets dans une keyframe et la segmentation d'instances.
        :param img: keyframe (np.ndarray)
        :return: la sortie brute du modèle
        """

        outputs = self.__instance_seg.segmentFrame(img.copy(), segment_target_classes=self.__target_classes,
                                                   extract_segmented_objects=True, mask_points_values=True,
                                                   extract_from_box=True)[0]
        return outputs


    def post_process(self, img, outputs, kf_detections):
        """
        Méthode effectuant le post-processing (traitement de la sortie du modèle) et la mise à jour des détections
        dans l'instance courante de KeyFrameDetections.
        :param img: kframe (np.ndarray)
        :param outputs: sortie brute du modèle
        :param kf_detections: instance courante de KeyFrameDetections
        :return:
        """

        kf_detections.set_boxes(outputs["boxes"])  # [start.x, start.y, end.x, end.y] -> 0, 2, 1, 3 pour [x, y, w, h]
        kf_detections.set_labels(outputs["class_names"])
        kf_detections.set_object_counts(outputs["object_counts"])
        kf_detections.set_confs(outputs["scores"])
        kf_detections.set_polygons(outputs["masks"])
        masks = self.__get_masks_from_polys__(img.shape, outputs["masks"], outputs["boxes"])
        kf_detections.set_masks(masks)
        kf_detections.set_extracted_objects(outputs["extracted_objects"])


    @staticmethod
    def __get_masks_from_polys__(img_shape, polys, boxes):
        """
        Méthode effectuant la création d'un masque à partir d'un polygone.
        :param img_shape: dimensions de la keyframe courante (tuple)
        :param polys: polygone d'un objet détecté (np.ndarray)
        :param boxes: bounding boxe d'un objet détecté (np.ndarray)
        :return: un masque (np.ndarray)
        """

        masks = []
        for i in range(len(boxes)):
            mask = np.zeros((img_shape[0], img_shape[1]))
            cv2.fillPoly(mask, polys[i], 1)
            mask = mask[boxes[i][1]:boxes[i][3], boxes[i][0]:boxes[i][2]]
            masks.append(mask.astype(bool))
        return masks


class DepthDetector(metaclass=MetaSingleton):
    """
    Cette classe permet de générer une carte de la profondeur d'une image.
    """

    def __init__(self, weights_path):
        """
        Constructeur de la classe DepthDetector.
        :param weights_path: chemin relatif des poids du modèle (str)
        """

        self.depth_model = self.__load_model__(weights_path)


    @staticmethod
    def __load_model__(weights_path):
        """
        Méthode effectuant la création, la compilation et le chargement des poids du modèle.
        :param weights_path: chemin relatif des poids du modèle (str)
        :return: le modèle (tf.keras.Model)
        """

        model = DepthEstimate(backbone="densenet121", weights=None, freeze=False)
        model.build(input_shape=(None, None, None, 3))
        model.load_weights(weights_path)
        return model


    def infer(self, img):
        """
        Méthode permettant de réaliser le préprocessing et la propagation avant de la keyframe courante par le modèle.
        :param img: keyframe (np.ndarray)
        :return: carte de la profondeur de la keyframe (np.ndarray)
        """

        init_shape = img.shape[0: 2]
        img_copy = img.copy()
        img_copy = np.expand_dims(cv2.resize(img_copy.astype("float32"), (640, 480)) / 255, axis=0)
        output = self.depth_model.predict(img_copy)
        output = output.reshape((240, 320))
        min = np.min(output)
        max = np.max(output)
        output = (output - min) / (max - min) # normalization
        depth_map = cv2.resize(output, init_shape[::-1])
        return depth_map


    @staticmethod
    def __calc_mean_depth__(depth_map, bb, mask):
        """
        Méthode effectuant le calcul de la profondeur moyenne pour un objet.
        :param depth_map: carte de la profondeur de la keyframe (np.ndarray)
        :param bb: bounding box d'un objet (np.ndarray)
        :param mask: masque d'un objet (np.ndarray)
        :return: profondeur moyenne de l'objet (float)
        """
        return np.mean(depth_map[bb[1]:bb[3], bb[0]:bb[2]][mask])


    def post_process(self, depth_map, kf_detections):
        """
        Méthode effectuant l'actualisation des profondeurs moyennes des objets détectés dans l'instance courante de
        KeyFrameDetections.
        :param depth_map: carte de la profondeur de la keyframe (np.ndarray)
        :param kf_detections: instance courante de KeyFrameDetections
        """

        depths = []
        boxes = kf_detections.get_boxes()
        masks = kf_detections.get_masks()
        for idx in range(kf_detections.get_nb_det()):
            depths.append(self.__calc_mean_depth__(depth_map, boxes[idx], masks[idx]))
        kf_detections.set_depths(depths)


class PoseDetector(metaclass=MetaSingleton):
    """
    Cette classe permet de détecter les angles formés par les articulations d'une personne.
    """

    def __init__(self, conf=0.5, visibility_thresh=0.9):
        """
        Constructeur de la classe PoseDetector.
        :param conf: seuil de confiance pour la détection.
        :param visibility_thresh: seuil de visibilité des articulations.
        """

        self.mp_pose = mp.solutions.pose
        self.pose_image = self.mp_pose.Pose(static_image_mode=True, min_detection_confidence=conf)
        self.visibility_thresh = visibility_thresh


    def __get_key_points__(self, image):
        """
        Méthode effectuant la détection des coordonnées des points des articulations d'une personne.
        :param image: image de la personne (np.ndarray)
        :return: coordonnées des points des articulations
        """

        rgb_img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        kp = self.pose_image.process(rgb_img)
        return kp


    def __calc_angle__(self, landmarks, idx):
        """
        Méthode effectuant le calcul des angles à partir des coordonnées des points des articulations.
        :param landmarks: points des articulations
        :param idx: indices des points formant un angle à une articulation donnée
        :return: valeur de l'angle en degrés, -1 si la visibilité est mauvaise (int)
        """

        if all(landmarks[i].visibility > self.visibility_thresh for i in idx):
            a = np.array([landmarks[idx[0]].x, landmarks[idx[0]].y, landmarks[idx[0]].z])
            b = np.array([landmarks[idx[1]].x, landmarks[idx[1]].y, landmarks[idx[1]].z])
            c = np.array([landmarks[idx[2]].x, landmarks[idx[2]].y, landmarks[idx[2]].z])

            radians = np.arctan2(c[1] - b[1], c[0] - b[0]) - np.arctan2(a[1] - b[1], a[0] - b[0])
            angle = np.abs(radians * 180.0 / np.pi)

            if angle > 180.0:
                angle = 360 - angle

            return round(angle)
        return -1


    def infer(self, img, mask):
        """
        Méthode effectuant le preprocessing (application du masque à l'image de l'objet) et la détection à l'aide du
        modèle.
        :param img: image de l'objet (np.ndarray)
        :param mask: masque de l'objet (np.ndarray)
        :return: sortie brute du modèle
        """

        masked = cv2.bitwise_and(img, img, mask=mask.astype("uint8"))
        outputs = self.__get_key_points__(masked)
        return outputs


    def post_process(self, kf_detections, outputs, i):
        """
        Méthode effectuant le post-processing de la sortie brute du modèle et l'ajout des angles détectés à l'instance
        courante de KeyFrameDetections pour un objet.
        :param kf_detections: instance courante de KeyFrameDetections
        :param outputs: sortie brute du modèle
        :param i: indice de l'objet détecté
        :return:
        """

        if not outputs.pose_landmarks:
            angles = {
                "left_elbow": -1, "right_elbow": -1, "left_shoulder": -1, "right_shoulder": -1, "left_hip": -1,
                "right_hip": -1, "left_knee": -1, "right_knee": -1, "left_foot": -1, "right_foot": -1
            }
        else:
            landmarks = outputs.pose_landmarks.landmark
            angles = {
                "left_elbow": self.__calc_angle__(landmarks, [16, 14, 12]),
                "right_elbow": self.__calc_angle__(landmarks, [15, 13, 11]),
                "left_shoulder": self.__calc_angle__(landmarks, [14, 12, 24]),
                "right_shoulder": self.__calc_angle__(landmarks, [13, 11, 23]),
                "left_hip": self.__calc_angle__(landmarks, [12, 24, 26]),
                "right_hip": self.__calc_angle__(landmarks, [11, 23, 25]),
                "left_knee": self.__calc_angle__(landmarks, [24, 26, 28]),
                "right_knee": self.__calc_angle__(landmarks, [23, 25, 27]),
                "left_foot": self.__calc_angle__(landmarks, [26, 28, 32]),
                "right_foot": self.__calc_angle__(landmarks, [25, 27, 31])
            }
        kf_detections.add_attributes(angles, i)


class FacialAttributesDetector(metaclass=MetaSingleton):
    """
    Cette classe permet de détecter le genre, l'âge et l'ethnicité d'une personne à partir du visage.
    """

    def __init__(self, face_attributes_path):
        """
        Constructeur de la classe FacialAttributesDetector.
        :param face_attributes_path: chemin relatif des poids du modèle (str)
        """
        self.recognition_model = face_recognition
        self.attributes_model = FaceAttributesModel()
        self.attributes_model.build(input_shape=(None, None, None, 3))
        self.attributes_model.load_weights(face_attributes_path)
        self.gender_classes = ['Male', 'Female']
        self.ethnicity_classes = ['White', 'Black', 'Asian', 'Indian', 'Others']


    def infer(self, img, mask):
        """
        Méthode effectuant la détection des attributs pour une personne.
        :param img: image du visage de la personne (np.ndarray)
        :param mask: masque pour le visage de la personne (np.ndarray)
        :return: sortie brute du modèle
        """

        masked = cv2.bitwise_and(img, img, mask=mask.astype("uint8"))
        det = self.recognition_model.face_locations(masked)
        if det:
            bb = det[0]
            face_img = masked[bb[0]:bb[2], bb[3]:bb[1]]
            face_grey = cv2.cvtColor(face_img, cv2.COLOR_BGR2GRAY)
            face_grey = cv2.cvtColor(face_grey, cv2.COLOR_GRAY2RGB)
            face_grey = cv2.resize(face_grey.astype(np.uint8), (48, 48))
            face_grey = np.expand_dims(face_grey, axis=0)
            outputs = self.attributes_model.predict(face_grey)
        else:
            outputs = False
        return outputs


    def post_process(self, kf_detections, outputs, i):
        """
        Méthode effectuant le post-processing de la sortie brute du modèle et l'actualisation des attributs dans
        l'instance courante de KeyFrameDetections.
        :param kf_detections: instance courante de KeyFrameDetections
        :param outputs: sortie brute du modèle
        :param i: indice de la personne
        """

        if outputs:
            face_attributes = {
                'face_visibility': True,
                'gender': self.gender_classes[round(outputs[2][0][0])],
                'age': round(outputs[0][0][0]),
                'ethnicity': self.ethnicity_classes[np.argmax(outputs[1])],
            }
        else:
            face_attributes = {
                'face_visibility': False
            }
        kf_detections.add_attributes(face_attributes, i)


class CarAttributesDetector(metaclass=MetaSingleton):
    """
    Cette classe permet de détecter le modèle d'une voiture.
    """

    def __init__(self, weights_path, classes_path):
        """
        Constructeur de la classe CarAttributesDetector.
        :param weights_path: chemin relatif des poids du modèle
        :param classes_path: chemin relatif des classes du modèle
        """

        self.car_model = ResNet_SWP(num_classes=196)
        self.car_model.build(input_shape=(None, 160, 160, 3))
        self.car_model.load_weights(weights_path)
        names_df = pd.read_csv(classes_path, header=None, names=["classes"])
        self.classes = names_df["classes"].tolist()


    def infer(self, img, mask):
        """
        Méthode effectuant le preprocessing et les prédictions du modèle sur l'image de la voiture.
        :param img: image de la voiture (np.ndarray)
        :param mask: masque de la voiture (np.ndarray)
        :return: sorties brutes
        """

        masked = cv2.bitwise_and(img, img, mask=mask.astype("uint8"))
        resized = cv2.resize(masked, (160, 160)).astype(np.int32)
        resized = np.expand_dims(resized, axis=0)
        outputs = self.car_model.predict(resized)
        return outputs


    def post_process(self, kf_detections, outputs, i):
        """
        Méthode effectuant le post-processing et l'actualisation des modèles des voitures détectés dans l'instance
        courante de KeyFrameDetections.
        :param kf_detections: instance courante de KeyFrameDetections
        :param outputs: sortie brute du modèle
        :param i: indice de la voiture
        """

        car_attributes = {
            'model': self.classes[np.argmax(outputs)],
        }
        kf_detections.add_attributes(car_attributes, i)
